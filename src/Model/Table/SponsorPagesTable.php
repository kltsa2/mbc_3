<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SponsorPages Model
 *
 * @property \App\Model\Table\SponsorsTable|\Cake\ORM\Association\BelongsTo $Sponsors
 *
 * @method \App\Model\Entity\SponsorPage get($primaryKey, $options = [])
 * @method \App\Model\Entity\SponsorPage newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SponsorPage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SponsorPage|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SponsorPage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SponsorPage[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SponsorPage findOrCreate($search, callable $callback = null, $options = [])
 */
class SponsorPagesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('sponsor_pages');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Sponsors', [
            'foreignKey' => 'sponsors_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('main_header')
            ->requirePresence('main_header', 'create')
            ->notEmpty('main_header');

        $validator
            ->scalar('main_header_desc')
            ->requirePresence('main_header_desc', 'create')
            ->notEmpty('main_header_desc');

        $validator
            ->scalar('second_header')
            ->requirePresence('second_header', 'create')
            ->notEmpty('second_header');

        $validator
            ->scalar('background_img_name')
            ->requirePresence('background_img_name', 'create')
            ->notEmpty('background_img_name');

        $validator
            ->scalar('path')
            ->requirePresence('path', 'create')
            ->notEmpty('path');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['sponsors_id'], 'Sponsors'));

        return $rules;
    }
}
