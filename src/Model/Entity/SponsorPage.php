<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SponsorPage Entity
 *
 * @property int $id
 * @property string $main_header
 * @property string $main_header_desc
 * @property string $second_header
 * @property string $background_img_name
 * @property string $path
 * @property int $sponsors_id
 *
 * @property \App\Model\Entity\Sponsor $sponsor
 */
class SponsorPage extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
