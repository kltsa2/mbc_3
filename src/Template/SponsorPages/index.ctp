<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\SponsorPage[]|\Cake\Collection\CollectionInterface $sponsorPages
  */
$this->layout = 'default';
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Sponsor Page'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sponsors'), ['controller' => 'Sponsors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sponsor'), ['controller' => 'Sponsors', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="sponsorPages index large-9 medium-8 columns content">
    <h3><?= __('Sponsor Pages') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('main_header') ?></th>
                <th scope="col"><?= $this->Paginator->sort('main_header_desc') ?></th>
                <th scope="col"><?= $this->Paginator->sort('second_header') ?></th>
                <th scope="col"><?= $this->Paginator->sort('background_img_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('path') ?></th>
                <th scope="col"><?= $this->Paginator->sort('sponsors_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($sponsorPages as $sponsorPage): ?>
            <tr>
                <td><?= $this->Number->format($sponsorPage->id) ?></td>
                <td><?= h($sponsorPage->main_header) ?></td>
                <td><?= h($sponsorPage->main_header_desc) ?></td>
                <td><?= h($sponsorPage->second_header) ?></td>
                <td><?= h($sponsorPage->background_img_name) ?></td>
                <td><?= h($sponsorPage->path) ?></td>
                <td><?= $sponsorPage->has('sponsor') ? $this->Html->link($sponsorPage->sponsor->name, ['controller' => 'Sponsors', 'action' => 'view', $sponsorPage->sponsor->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $sponsorPage->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $sponsorPage->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $sponsorPage->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sponsorPage->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
