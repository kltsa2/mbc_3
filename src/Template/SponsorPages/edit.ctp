<?php
/**
  * @var \App\View\AppView $this
  */
$this->layout = 'default';
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $sponsorPage->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $sponsorPage->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Sponsor Pages'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Sponsors'), ['controller' => 'Sponsors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sponsor'), ['controller' => 'Sponsors', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="sponsorPages form large-9 medium-8 columns content">
    <?= $this->Form->create($sponsorPage) ?>
    <fieldset>
        <legend><?= __('Edit Sponsor Page') ?></legend>
        <?php
            echo $this->Form->control('main_header');
            echo $this->Form->control('main_header_desc');
            echo $this->Form->control('second_header');
            echo $this->Form->control('background_img_name');
            echo $this->Form->control('path');
            echo $this->Form->control('sponsors_id', ['options' => $sponsors]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
