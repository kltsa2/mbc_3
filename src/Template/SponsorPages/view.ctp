<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\SponsorPage $sponsorPage
  */
$this->layout = 'default';
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Sponsor Page'), ['action' => 'edit', $sponsorPage->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Sponsor Page'), ['action' => 'delete', $sponsorPage->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sponsorPage->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Sponsor Pages'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sponsor Page'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sponsors'), ['controller' => 'Sponsors', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sponsor'), ['controller' => 'Sponsors', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="sponsorPages view large-9 medium-8 columns content">
    <h3><?= h($sponsorPage->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Main Header') ?></th>
            <td><?= h($sponsorPage->main_header) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Main Header Desc') ?></th>
            <td><?= h($sponsorPage->main_header_desc) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Second Header') ?></th>
            <td><?= h($sponsorPage->second_header) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Background Img Name') ?></th>
            <td><?= h($sponsorPage->background_img_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Path') ?></th>
            <td><?= h($sponsorPage->path) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sponsor') ?></th>
            <td><?= $sponsorPage->has('sponsor') ? $this->Html->link($sponsorPage->sponsor->name, ['controller' => 'Sponsors', 'action' => 'view', $sponsorPage->sponsor->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($sponsorPage->id) ?></td>
        </tr>
    </table>
</div>
