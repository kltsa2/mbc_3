<?php
/**
  * @var \App\View\AppView $this
 *  @var \App\Model\Entity\Sponsor $sponsor
  */
$this->layout = 'default';
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $sponsor->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $sponsor->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Sponsors'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="sponsors form large-9 medium-8 columns content">
    <?= $this->Form->create($sponsor) ?>
    <fieldset>
        <legend><?= __('Edit Sponsor') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('description');
            echo $this->Form->control('sponsor_url');


        ?>
    </fieldset>
    <?php echo $this->Form->create($uploadData, ['type' => 'file']);?>
    <?php echo $this->Form->input('file', ['type' => 'file', 'class' => 'form-control']);?>
    <?= $this->Form->button(__('Submit'),['type'=>'submit', 'class' => 'form-controlbtn btn-default']); ?>
    <?= $this->Form->end() ?>
</div>
