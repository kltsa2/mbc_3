<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Sponsor $sponsors
 */

?>


<h1>Add Sponsor</h1>
<div class="content">
    <?= $this->Flash->render() ?>

        <?= $this->Form->create($sponsors, ['type' => 'file']); ?>

        <FIELDSET>
        <?php echo $this->Form->control('Sponsor_name', ['type' => 'text']); ?>
        <?php echo $this->Form->control('description', ['type' => 'text']); ?>
        <?php echo $this->Form->control('sponsor_url', ['type' => 'text']); ?>
        </FIELDSET>

        <h3>Upload Sponsor Image</h3>
        <?php echo $this->Form->create($uploadData, ['type' => 'file']); ?>
        <?php echo $this->Form->input('file', ['type' => 'file', 'class' => 'form-control']); ?>
        <?php echo $this->Form->button(__('Add New Sponsor'), ['type'=>'submit', 'class' => 'form-controlbtn btn-default']); ?>
        <?php echo $this->Form->end(); ?>

</div>



<div class="content">
    <!-- Table -->
    <table class="table">
        <tr>
            <th width="20%">Name</th>
            <th width="20%">Sponsor Name</th>
            <th width="20%">Sponsor Image</th>

            <th width="12%">Upload Date</th>
            <th width="12%">Actions</th>
        </tr>
        <?php if($sponsorsRowNum > 0):$count = 0; foreach($sponsors as $sponsor): $count++;?>
            <tr>
                <td><?= $sponsor->sponsor_img_name ?></td>
                <td><?= $sponsor->name;?></td>
                <td><embed src="<?= $sponsor->path.$sponsor->sponsor_img_name ?>" width="250px" height="200px"></td>

                <td><?php echo $sponsor->created; ?></td>

                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $sponsor->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $sponsor->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $sponsor->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sponsor->id)]) ?>
                </td>


            </tr>

        <?php endforeach; else:?>
            <tr><td colspan="3">No file(s) found......</td></tr>
        <?php endif; ?>


    </table>
</div>