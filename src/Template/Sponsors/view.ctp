<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Sponsor $sponsor
  */
$this->layout = 'default';
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Sponsor'), ['action' => 'edit', $sponsor->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Sponsor'), ['action' => 'delete', $sponsor->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sponsor->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Sponsors'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sponsor'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="sponsors view large-9 medium-8 columns content">
    <h3><?= h($sponsor->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($sponsor->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sponsor Url') ?></th>
            <td><?= h($sponsor->sponsor_url) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sponsor Img Name') ?></th>
            <td><?= h($sponsor->sponsor_img_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Path') ?></th>
            <td><?= h($sponsor->path) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($sponsor->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($sponsor->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($sponsor->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $sponsor->status ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($sponsor->description)); ?>
    </div>
</div>
