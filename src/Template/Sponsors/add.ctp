<?php
/**
  * @var \App\View\AppView $this
  */
$this->layout = 'default';
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Sponsors'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="sponsors form large-9 medium-8 columns content">
    <?= $this->Form->create($sponsor) ?>
    <fieldset>
        <legend><?= __('Add Sponsor') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('description');
            echo $this->Form->control('sponsor_url');
            echo $this->Form->control('sponsor_img_name');
            echo $this->Form->control('path');
            echo $this->Form->control('status');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
