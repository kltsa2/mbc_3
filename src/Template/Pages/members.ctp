<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author"      content="Sergey Pozhilov (GetTemplate.com)">

	<title>Blog - Progressus Bootstrap template</title>

</head>

<body class="page-blog">
    <!-- navbar -->
    <div class= "navbar navbar-dual navbar-inverse navbar-fixed-top headroom ontop-now">
        <div class="container">
            <div class="navbar-header">
                <!-- Button for smallest screens -->
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <a class="navbar-brand" href="index.ctp">
                    <!--                <img src="../webroot/img/logos/test-1.jpg" alt="Progressus HTML5 template">-->
                    <!--                <img src="../webroot/img/logos/mbc.png" alt="Progressus HTML5 template" class="secondary">-->
                </a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav pull-right">
                    <li><a href="index">Home</a></li>
                    <li><a href="about">About</a></li>
                    <li><a href="venue">Venue</a></li>
                    <li class="active"><a href="members">Members</a></li>
                    <li><a href="sponsors">Sponsors</a></li>
                    <li><a href="contact">Contact</a></li>
                    <li><a class="btn btn-rounded" href="signin.html">SIGN IN</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end of navbar -->

    <!-- header -->
	<header class="head-inner">
		<div class="container">
			<div class="row">
				<h1 class="page-title text-center">Progressus Blog</h1>
				<h2 class="page-lead text-center">Fresh news, updates, tricks and case studies</h2>
			</div>
		</div>
	</header>
	<!-- end of header -->


	<!-- posts section -->
	<section class="section section-blog">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">

					<!-- post -->
					<article class="post">
						<div class="entry-header">
							<h1 class="entry-title"><a href="single.ctp" rel="bookmark">Hello world!</a></h1>
							<div class="entry-meta">
								<span class="posted-on"><time class="entry-date published" date="2015-04-14">Posted by <a href="">Sergey</a> <span class="meta-divider">•</span> April 14, 2015</time></span>
							</div>
						</div>
						<div class="entry-content">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius, molestias, architecto, adipisci, numquam dolor iusto eos reprehenderit placeat quam debitis quas magni eveniet. Saepe, nam, iste consectetur quae necessitatibus dolores provident veritatis possimus rerum facilis quia dicta itaque sapiente iusto natus quidem magni quibusdam. Explicabo nesciunt vel rem obcaecati reprehenderit eveniet culpa repudiandae. Distinctio, quia, provident illum necessitatibus repellendus rem voluptates exercitationem numquam inventore itaque atque sint nihil eveniet consequuntur eius! Laborum, at sit animi quae quidem ex tempora facilis.</p>
							<p><a href="single.ctp" class="more-link">Continue reading</a></p>
						</div>
					</article>
					<!-- /post -->

					<!-- post -->
	 				<article class="post">
						<div class="entry-header">
	 						<h1 class="entry-title"><a href="single.ctp" rel="bookmark">Vivamus Lacus Mauris</a></h1>
	 						<div class="entry-meta">
	 							<span class="posted-on"><time class="entry-date published" date="2015-04-14">Posted by <a href="">Sergey</a> <span class="meta-divider">•</span> April 14, 2015</time></span>
	 						</div>
						</div>
						<div class="entry-content">
							<p><img alt="" src="assets/images/mac.jpg"></p>
							<p>Mauris eget quam orci. Quisque porta varius dui, quis posuere nibh mollis quis. Mauris commodo rhoncus porttitor. Maecenas et euismod elit. Nulla facilisi. Vivamus lacus libero, ultrices non ullamcorper ac, tempus sit amet enim. Suspendisse at semper ipsum. Suspendisse sagittis diam a massa viverra sollicitudin. Vivamus sagittis est eu diam fringilla nec tristique metus vestibulum. Donec magna purus, pellentesque vel lobortis ut, convallis id augue. Sed odio magna, pellentesque eget convallis ac, vehicula vel arcu. Sed eu scelerisque dui. Sed eu arcu at nibh hendrerit viverra. Vivamus lacus augue, sodales id cursus in, condimentum at risus. </p>
							<p><a href="single.ctp" class="more-link">Continue reading</a></p>
						</div>
					</article>
					<!-- /post -->

					<!-- post -->
					<article class="post">
						<div class="entry-header">
	 						<h1 class="entry-title"><a href="single.ctp" rel="bookmark">Vivamus Lacus Mauris</a></h1>
	 						<div class="entry-meta">
	 							<span class="posted-on"><time class="entry-date published" date="2015-04-14">Posted by <a href="">Sergey</a> <span class="meta-divider">•</span> April 14, 2015</time></span>
	 						</div>
						</div>
						<div class="entry-content">
							<p>Mauris eget quam orci. Quisque porta varius dui, quis posuere nibh mollis quis. Mauris commodo rhoncus porttitor. Maecenas et euismod elit. Nulla facilisi. Vivamus lacus libero, ultrices non ullamcorper ac, tempus sit amet enim. Suspendisse at semper ipsum. Suspendisse sagittis diam a massa viverra sollicitudin. Vivamus sagittis est eu diam fringilla nec tristique metus vestibulum. Donec magna purus, pellentesque vel lobortis ut, convallis id augue. Sed odio magna, pellentesque eget convallis ac, vehicula vel arcu. Sed eu scelerisque dui. Sed eu arcu at nibh hendrerit viverra. Vivamus lacus augue, sodales id cursus in, condimentum at risus.</p>
							<p><a href="single.ctp" class="more-link">Continue reading</a></p>
						</div>
					</article>
					<!-- /post -->

					<!-- post -->
					<article class="post">
						<div class="entry-header">
							<h1 class="entry-title"><a href="single.ctp" rel="bookmark">Maecenas Quisque Suspendisse Lorem</a></h1>
							<div class="entry-meta">
								<span class="posted-on"><time class="entry-date published" date="2015-04-14">Posted by <a href="">Sergey</a> <span class="meta-divider">•</span> April 14, 2015</time></span>
							</div>
						</div>
						<div class="entry-content">
							<p>Mauris eget quam orci. Quisque porta varius dui, quis posuere nibh mollis quis. Mauris commodo rhoncus porttitor. Maecenas et euismod elit. Nulla facilisi. Vivamus lacus libero, ultrices non ullamcorper ac, tempus sit amet enim. Suspendisse at semper ipsum. Suspendisse sagittis diam a massa viverra sollicitudin. Vivamus sagittis est eu diam fringilla nec tristique metus vestibulum. Donec magna purus, pellentesque vel lobortis ut, convallis id augue. Sed odio magna, pellentesque eget convallis ac, vehicula vel arcu. Sed eu scelerisque dui. Sed eu arcu at nibh hendrerit viverra. Vivamus lacus augue, sodales id cursus in, condimentum at risus.</p>
							<p><a href="single.ctp" class="more-link">Continue reading</a></p>
						</div>
	 				</article>
	 				<!-- /post -->

					<!-- post -->
					<article class="post">
						<div class="entry-header">
							<h1 class="entry-title"><a href="single.ctp" rel="bookmark">Pellentesque Eget Convallis</a></h1>
	 						<div class="entry-meta">
	 							<span class="posted-on"><time class="entry-date published" date="2015-04-14">Posted by <a href="">Sergey</a> <span class="meta-divider">•</span> April 14, 2015</time></span>
	 						</div>
						</div>
						<div class="entry-content">
							<p>Mauris eget quam orci. Quisque porta varius dui, quis posuere nibh mollis quis. Mauris commodo rhoncus porttitor. Maecenas et euismod elit. Nulla facilisi. Vivamus lacus libero, ultrices non ullamcorper ac, tempus sit amet enim. Suspendisse at semper ipsum. Suspendisse sagittis diam a massa viverra sollicitudin. Vivamus sagittis est eu diam fringilla nec tristique metus vestibulum. Donec magna purus, pellentesque vel lobortis ut, convallis id augue. Sed odio magna, pellentesque eget convallis ac, vehicula vel arcu. Sed eu scelerisque dui. Sed eu arcu at nibh hendrerit viverra. Vivamus lacus augue, sodales id cursus in, condimentum at risus.</p>
							<p><a href="single.ctp" class="more-link">Continue reading</a></p>
						</div>
					</article>
					<!-- /post -->

					<!-- previous/next page links -->
					<div class="row">
						<div class="col-sm-6">
							<a href="" class="btn btn-action btn-ghost btn-rounded"><i class="fa fa-long-arrow-left"></i> Previous entries</a>
						</div>
						<div class="col-sm-6 text-right">
							<!-- <a href="" class="btn btn-action btn-ghost btn-rounded">Newer entries <i class="fa fa-long-arrow-right"></i></a> -->
						</div>
					</div>

				</div>
			</div>
		</div>
	</section>
	<!-- end of posts section -->


	<!-- subscribe to updates section -->
	<section class="section jumbotron bottomspace-0">
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-sm-push-2">
					<h3>Don't miss any update</h3>
					<p>Subscribe to our newsletter. Don't worry, we're not going to spam you )</p>
				</div>
				<div class="col-sm-3 col-sm-push-3">
					<div class="input-group topspace-2x">
						<input type="text" class="form-control" placeholder="your@email">
						<span class="input-group-btn">
							<button class="btn btn-default" type="button">Subscribe</button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- end of subscribe to updates section -->
</body>
</html>
