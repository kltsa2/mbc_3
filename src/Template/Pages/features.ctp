<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author"      content="Sergey Pozhilov (GetTemplate.com)">

	<title>Features - Progressus Bootstrap template</title>

</head>

<body class="page-features">

	<!-- fixed navbar -->
<!--	<div class= "navbar navbar-dual navbar-inverse navbar-fixed-top headroom ontop-now">-->
<!--		<div class="container">-->
<!--			<div class="navbar-header">-->
<!--				<!-- Button for smallest screens -->
<!--				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>-->
<!--				<a class="navbar-brand" href="index.ctp">-->
<!--					<img src="assets/images/logo.png" alt="Progressus HTML5 template">-->
<!--					<img src="assets/images/logo_on_white.png" alt="Progressus HTML5 template" class="secondary">-->
<!--				</a>-->
<!--			</div>-->
<!--			<div class="navbar-collapse collapse">-->
<!--				<ul class="nav navbar-nav pull-right">-->
<!--					<li><a href="index.ctp">Home</a></li>-->
<!--					<li><a href="landingstartup.ctp">Landing</a></li>-->
<!--					<li class="active"><a href="features.ctp">Features</a></li>-->
<!--					<li><a href="pricing.ctp">Pricing</a></li>-->
<!--					<li class="dropdown">-->
<!--						<a href="#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">More Pages <b class="caret"></b></a>-->
<!--						<ul class="dropdown-menu">-->
<!--							<li><a href="services.ctp">Services</a></li>-->
<!--							<li><a href="portfolio.ctp">Portfolio</a></li>-->
<!--							<li><a href="portfolioitem.ctp">Portfolio Item</a></li>-->
<!--							<li><a href="blog.ctp">Blog Home</a></li>-->
<!--							<li><a href="single.ctp">Single Post</a></li>-->
<!--							<li><a href="about.html">About</a></li>-->
<!--							<li><a href="sidebarleft.ctp">Left Sidebar</a></li>-->
<!--							<li><a href="sidebarright.ctp">Right Sidebar</a></li>-->
<!--							<li><a href="contact.ctp">Contact</a></li>-->
<!--						</ul>-->
<!--					</li>-->
<!--					<li><a class="btn btn-rounded" href="signin.ctp">SIGN IN / SIGN UP</a></li>-->
<!--				</ul>-->
<!--			</div><!--/.nav-collapse -->
<!--		</div>-->
<!--	</div>-->
	<!-- end of fixed navbar -->


	<!-- header -->
	<header class="head-inner">
		<div class="container">
			<div class="row">
				<h1 class="page-title text-center">Features</h1>
				<h2 class="page-lead text-center">Meet the first bullsh*t-free premium Bootstrap template<br>made exclusively for outstanding companies.</h2>
			</div>
		</div>
	</header>
	<!-- end of header -->


	<!-- mission section -->
	<section class="section section-mission topspace-2x">
		<div class="container">
			<div class="row">
				<div class="col-sm-10 col-sm-push-1">
					<p class="lead text-center">Our mission is... lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis odit beatae, consequatur adipisci quisquam minima atque, illo, impedit hic obcaecati sed sunt asperiores maiores aperiam aliquid.</p>
				</div>
			</div>
		</div>
	</section>
	<!-- end of mission section -->


	<!-- features section -->
	<section class="section section-features info-icons-h">
		<div class="container">
			<div class="row">
				<figure class="col-sm-4 col-sm-push-2 text-right">
					<i class="icon fa fa-gift color-action"></i>
					<div class="text">
						<h3 class="caption">Lorem ipsum</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint unde eaque officia iste quibusdam nostrum, dicta excepturi libero, obcaecati eum.</p>
					</div>
				</figure>
				<figure class="col-sm-4 col-sm-push-2">
					<i class="icon fa fa-globe color-action"></i>
					<div class="text">
						<h3 class="caption">Consectetur adipisicing</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam repellendus recusandae voluptatum quaerat nemo totam, laudantium atque laboriosam fugiat accusamus.</p>
					</div>
				</figure>
			</div> <!-- /row -->
			<div class="row topspace">
				<figure class="col-sm-4 col-sm-push-2 text-right">
					<i class="icon fa fa-lock color-action"></i>
					<div class="text">
						<h3 class="caption">Vitae, facilis.</h3>
						<p>In quia ex pariatur, tempora necessitatibus animi, id consequatur sed ipsa corrupti doloribus nam, harum ut modi molestias vero! Dicta.</p>
					</div>
				</figure>
				<figure class="col-sm-4 col-sm-push-2">
					<i class="icon fa fa-cogs color-action"></i>
					<div class="text">
						<h3 class="caption">Ipsa, et!</h3>
						<p>Voluptate possimus earum excepturi nemo quae, asperiores sapiente, vel natus. Qui omnis enim harum! Fugiat rerum placeat in repudiandae recusandae?</p>
					</div>
				</figure>
			</div> <!-- /row -->
			<div class="row topspace">
				<figure class="col-sm-4 col-sm-push-2 text-right">
					<i class="icon fa fa-shopping-cart color-action"></i>
					<div class="text">
						<h3 class="caption">Doloremque, sequi?</h3>
						<p>Minus illo mollitia repellendus beatae, consectetur nemo labore tempore at blanditiis harum laudantium ducimus vero consequuntur similique sit in eos.</p>
					</div>
				</figure>
				<figure class="col-sm-4 col-sm-push-2">
					<i class="icon fa fa-shield color-action"></i>
					<div class="text">
						<h3 class="caption">Quaerat, odit.</h3>
						<p>Amet minima dolorem dolorum, ipsa esse doloremque, rerum necessitatibus repellat ipsam sint quas explicabo earum corrupti ea sequi. Voluptatem, assumenda.</p>
					</div>
				</figure>
			</div> <!-- /row -->
			<div class="row topspace">
				<figure class="col-sm-4 col-sm-push-2 text-right">
					<i class="icon fa fa-star color-action"></i>
					<div class="text">
						<h3 class="caption">Voluptatum, suscipit.</h3>
						<p>Nisi alias hic corrupti ducimus odio ipsam quas animi dolore at magni laboriosam ipsa reprehenderit excepturi, reiciendis nobis neque iusto.</p>
					</div>
				</figure>
				<figure class="col-sm-4 col-sm-push-2">
					<i class="icon fa fa-check color-action"></i>
					<div class="text">
						<h3 class="caption">Harum, nulla?</h3>
						<p>Sint quam accusantium magni est. Culpa nesciunt, fugiat atque minus, porro incidunt iusto voluptatum, unde tenetur natus molestias deleniti molestiae.</p>
					</div>
				</figure>
			</div> <!-- /row -->
			<div class="row topspace">
				<figure class="col-sm-4 col-sm-push-2 text-right">
					<i class="icon fa fa-heart color-action"></i>
					<div class="text">
						<h3 class="caption">Debitis, numquam.</h3>
						<p>Itaque debitis sapiente repudiandae quis, officia fugit sequi, aliquid necessitatibus. Iure dignissimos ratione sapiente autem labore earum eligendi voluptate voluptates.</p>
					</div>
				</figure>
				<figure class="col-sm-4 col-sm-push-2">
					<i class="icon fa fa-random color-action"></i>
					<div class="text">
						<h3 class="caption">Corporis, obcaecati.</h3>
						<p>Debitis nesciunt ipsum perspiciatis rem similique sed est, vel, exercitationem inventore laboriosam, consequuntur tenetur ea dignissimos! Esse quisquam, quas autem.</p>
					</div>
				</figure>
			</div>
		</div>
	</section>
	<!-- end of features section -->


	<!-- logotypes section -->
	<section class="section section-logos jumbotron">
		<div class="container">
			<h2 class="title text-center">Happy customers</h2>
			<div class="row topspace">
				<div class="col-lg-2 col-sm-3 text-center"><img width="140" src="assets/images/logos/1.png" alt=""></div>
				<div class="col-lg-2 col-sm-3 text-center"><img width="140" src="assets/images/logos/2.png" alt=""></div>
				<div class="col-lg-2 col-sm-3 text-center"><img width="140" src="assets/images/logos/3.png" alt=""></div>
				<div class="col-lg-2 col-sm-3 text-center"><img width="140" src="assets/images/logos/4.png" alt=""></div>
				<div class="col-lg-2 col-sm-3 text-center"><img width="140" src="assets/images/logos/5.png" alt=""></div>
				<div class="col-lg-2 col-sm-3 text-center"><img width="140" src="assets/images/logos/6.png" alt=""></div>
			</div>

			<hr class="topspace">

			<div class="section-testimonials">
				<div id="testimonials-carousel" class="carousel slide topspace-2x" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#testimonials-carousel" data-slide-to="0" class="active"></li>
						<li data-target="#testimonials-carousel" data-slide-to="1"></li>
						<li data-target="#testimonials-carousel" data-slide-to="2"></li>
					</ol>
					<!--//carousel-indicators-->
					<div class="carousel-inner">
						<div class="row item active">
							<div class="col-sm-3 profile">
								<img src="assets/images/people/1.jpg" alt="" />
							</div>
							<div class="col-sm-9 content">
								<blockquote>
									<i class="fa fa-quote-left"></i>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam deserunt accusamus soluta maiores alias, aliquid ipsum nobis laudantium qui, eos architecto accusantium doloremque dolorem quia asperiores voluptate pariatur. Laboriosam, ullam.</p>
								</blockquote>
								<p class="source">Mario Hayes
									<br/><span class="title">Co-Founder, Superawesome Inc</span>
								</p>
							</div>
							<!--//content-->
						</div>
						<!--//item-->
						<div class="row item">
							<div class="col-sm-3 profile">
								<img src="assets/images/people/2.jpg" alt="" />
							</div>
							<div class="col-sm-9 content">
								<blockquote>
									<i class="fa fa-quote-left"></i>
									<p>Ipsa nemo, minus perspiciatis harum at, repudiandae quae aliquam quo? Ullam laborum unde corporis eos ipsa esse necessitatibus in natus atque labore delectus, aperiam perferendis, assumenda iusto qui cum dolor!</p>
								</blockquote>
								<p class="source">Erin Mendoza
									<br/><span class="title">Entrepreneur, GT corp</span>
								</p>
							</div>
							<!--//content-->
						</div>
						<!--//item-->
						<div class="row item">
							<div class="col-sm-3 profile">
								<img src="assets/images/people/3.jpg" alt="" />
							</div>
							<div class="col-sm-9 content">
								<blockquote>
									<i class="fa fa-quote-left"></i>
									<p>Voluptatibus laboriosam pariatur doloribus repudiandae blanditiis cum quibusdam similique nisi nemo labore eos sed quasi quia, assumenda, ratione sit provident beatae hic eius velit neque magni distinctio ab quaerat ipsa!</p>
								</blockquote>
								<p class="source">Dwight Ray
									<br/><span class="title">CTO, Drave</span>
								</p>
							</div>
							<!--//content-->
						</div>
						<!--//item-->
					</div>
					<!--//carousel-inner-->
				</div>
				<!--//carousel-->
			</div>

		</div>
	</section>
	<!-- end of logotypes section -->


	<section class="section">
		<div class="container">
			<h4 class="text-center text-uppercase bottomspace">Download this awesome Bootstrap template today</h4>
			<p class="text-center"><a href="" class="btn btn-lg btn-action">Download</a></p>
		</div>
	</section>

</body>
</html>
