<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SponsorPages Controller
 *
 * @property \App\Model\Table\SponsorPagesTable $SponsorPages
 *
 * @method \App\Model\Entity\SponsorPage[] paginate($object = null, array $settings = [])
 */
class SponsorPagesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Sponsors']
        ];
        $sponsorPages = $this->paginate($this->SponsorPages);

        $this->set(compact('sponsorPages'));
        $this->set('_serialize', ['sponsorPages']);
    }

    /**
     * View method
     *
     * @param string|null $id Sponsor Page id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $sponsorPage = $this->SponsorPages->get($id, [
            'contain' => ['Sponsors']
        ]);

        $this->set('sponsorPage', $sponsorPage);
        $this->set('_serialize', ['sponsorPage']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $sponsorPage = $this->SponsorPages->newEntity();
        if ($this->request->is('post')) {
            $sponsorPage = $this->SponsorPages->patchEntity($sponsorPage, $this->request->getData());
            if ($this->SponsorPages->save($sponsorPage)) {
                $this->Flash->success(__('The sponsor page has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sponsor page could not be saved. Please, try again.'));
        }
        $sponsors = $this->SponsorPages->Sponsors->find('list', ['limit' => 200]);
        $this->set(compact('sponsorPage', 'sponsors'));
        $this->set('_serialize', ['sponsorPage']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sponsor Page id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $sponsorPage = $this->SponsorPages->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $sponsorPage = $this->SponsorPages->patchEntity($sponsorPage, $this->request->getData());
            if ($this->SponsorPages->save($sponsorPage)) {
                $this->Flash->success(__('The sponsor page has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sponsor page could not be saved. Please, try again.'));
        }
        $sponsors = $this->SponsorPages->Sponsors->find('list', ['limit' => 200]);
        $this->set(compact('sponsorPage', 'sponsors'));
        $this->set('_serialize', ['sponsorPage']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Sponsor Page id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $sponsorPage = $this->SponsorPages->get($id);
        if ($this->SponsorPages->delete($sponsorPage)) {
            $this->Flash->success(__('The sponsor page has been deleted.'));
        } else {
            $this->Flash->error(__('The sponsor page could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
