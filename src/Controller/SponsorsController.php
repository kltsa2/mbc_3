<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

/**
 * Sponsors Controller
 *
 * @property \App\Model\Table\SponsorsTable $Sponsors
 *
 * @method \App\Model\Entity\Sponsor[] paginate($object = null, array $settings = [])
 */
class SponsorsController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        // Include the FlashComponent
        $this->loadComponent('Flash');

        // Load Files model
        $this->loadModel('Sponsors');

        // Set the layout
        $this->layout = 'default';
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $sponsors = $this->paginate($this->Sponsors);

        $this->set(compact('sponsors'));
        $this->set('_serialize', ['sponsors']);


        $uploadData = '';

        if ($this->request->is('post')) {
            if (!empty($this->request->data['file']['name'])) {
                $sponsorName = $this->request->data['file']['name'];


                $sponsorDescription = 1;
                $sponsor_url = 1;

                $uploadPath = '../webroot/uploads/sponsors/';
                $uploadFile = $uploadPath . $sponsorName;
                if (move_uploaded_file($this->request->data['file']['tmp_name'], $uploadFile)) {
                    $uploadData = $this->Sponsors->newEntity();

                    $uploadData->name = $this->request->data('Sponsor_name');
                    $uploadData->description = $this->request->data('description');
                    $uploadData->sponsor_url = $this->request->data('sponsor_url');
                    $uploadData->sponsor_img_name = $sponsorName;
                    $uploadData->path = $uploadPath;
                    $uploadData->created = date("Y-m-d H:i:s");
                    $uploadData->modified = date("Y-m-d H:i:s");
                    $uploadData->status = 1;
                    if ($this->Sponsors->save($uploadData)) {
                        $this->Flash->success(__('File has been uploaded and inserted successfully.'));
                        $this->redirect('http://localhost/MBC_3/Sponsors/index');

                    } else {
                        $this->Flash->error(__('Unable to upload file, please try again.'));
                    }
                } else {
                    $this->Flash->error(__('Unable to upload file, please try again.'));
                }
            } else {
                $this->Flash->error(__('Please choose a file to upload.'));
            }

        }

        $this->set('uploadData', $uploadData);

        $sponsors = $this->Sponsors->find('all', ['order' => ['Sponsors.created' => 'DESC']]);
        $sponsorsRowNum = $sponsors->count();
        $this->set('sponsors', $sponsors);
        $this->set('sponsorsRowNum', $sponsorsRowNum);

    }


    /**
     * View method
     *
     * @param string|null $id Sponsor id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $sponsor = $this->Sponsors->get($id, [
            'contain' => []
        ]);

        $this->set('sponsor', $sponsor);
        $this->set('_serialize', ['sponsor']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $sponsor = $this->Sponsors->newEntity();
        if ($this->request->is('post')) {
            $sponsor = $this->Sponsors->patchEntity($sponsor, $this->request->getData());
            if ($this->Sponsors->save($sponsor)) {
                $this->Flash->success(__('The sponsor has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sponsor could not be saved. Please, try again.'));
        }
        $this->set(compact('sponsor'));
        $this->set('_serialize', ['sponsor']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sponsor id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {

        $sponsor = $this->Sponsors->get($id, [
            'contain' => []
        ]);


        if ($this->request->is(['patch', 'post', 'put'])) {


                    $sponsor = $this->Sponsors->patchEntity($sponsor, $this->request->getData());
                    if ($this->Sponsors->save($sponsor)) {
                        $this->Flash->success(__('The sponsor has been saved.'));

                        return $this->redirect(['action' => 'index']);
                    }
                    $this->Flash->error(__('The sponsor could not be saved. Please, try again.'));
                }
                $this->set(compact('sponsor'));
                $this->set('_serialize', ['sponsor']);
            }







    /**
     * Delete method
     *
     * @param string|null $id Sponsor id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id)
    {
        $this->request->allowMethod(['post', 'delete']);
        $sponsor = $this->Sponsors->get($id);
        if ($this->Sponsors->delete($sponsor)) {
            $dir = new Folder('../webroot/uploads/sponsors/');
            $file_to_delete = new File($dir->pwd() . DS . $sponsor->sponsor_img_name);
            $file_to_delete->delete();
            $this->Flash->success(__('The file has been deleted.'));
            $this->redirect('http://localhost/MBC_3/Sponsors/index');
        } else {
            $this->Flash->error(__('The file could not be deleted. Please, try again.'));
        }

    }


}
