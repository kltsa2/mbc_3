<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SponsorPagesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SponsorPagesTable Test Case
 */
class SponsorPagesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SponsorPagesTable
     */
    public $SponsorPages;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sponsor_pages',
        'app.sponsors'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SponsorPages') ? [] : ['className' => SponsorPagesTable::class];
        $this->SponsorPages = TableRegistry::get('SponsorPages', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SponsorPages);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
